from tkinter import *
from PIL import ImageTk,Image

"""
Interface used in image selection
"""

class RankingPair:
    """
    Class that adds Image-Checkbox pairs to the window
    """
    def __init__(self,window, img_file, row, column):
        self.name = img_file
        self.window = window
        self.row = row
        self.column = column

        self.img = ImageTk.PhotoImage(Image.open(img_file).resize((400, 400), Image.ANTIALIAS))
        self.label = Label(self.window, image=self.img).grid(row=row, column=self.column)

        self.var = StringVar()
        self.c_box = Checkbutton(
            window, text="Color image", variable=self.var,
            onvalue=self.name, offvalue= ""
        ).grid(row = self.row + 1, column = self.column)

    def setRank(self,rank):
        self.rank = rank


class Window():
    """
    Setup Interface
    """
    def __init__(self):

        self.root = Tk()

        self.eval_dict = {}
        self.ranks = []

        # Params that are changeable
        self.num_of_imgs = 12
        self.num_of_lines = 2

        for i in range(self.num_of_imgs):
            filename = "./Ideas/images/"+str(i)+".jpg"
            col = i if i < self.num_of_imgs / self.num_of_lines else i % (self.num_of_imgs / self.num_of_lines)
            rw = 0 if i < self.num_of_imgs / self.num_of_lines else self.num_of_lines
            # rw = 0
            self.eval_dict[filename] = RankingPair(self.root, filename, rw, col)
        Button(self.root, text="Submit", command=self.submit).grid(row=(self.num_of_imgs / self.num_of_lines) + 1, column=self.num_of_lines + 1)


    def validateEntry(self, entry):
        if entry.var.get() != "":
            return entry.name, True
        return -1, False

    def submit(self):
        """
        Used in the evolutionary process for selecting the two individuals that should mate
        Check if only two are selected.
        :return: Indices of the two selected Images
        """
        validatedList = []
        for entry in self.eval_dict.values():
            rank, validated = self.validateEntry(entry)
            if validated:
                self.ranks.append(rank)
                validatedList.append(validated)
        if len(validatedList) == 2:
            self.root.destroy()
        else:
            Message(self.root, text="You Have to select two pictures").grid(row=0, column= self.num_of_imgs + 1)
            validatedList = []

#building the window
def runWindow():
    window = Window()
    window.root.mainloop()
    return window.ranks