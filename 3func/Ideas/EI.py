from PIL import Image
from deap import base
from deap import creator
from deap import gp
from deap import tools
import numpy as np
import interface.Interface
import mathematics



class FunctionGenerator(gp.PrimitiveSet):

    def __init__(self, name, arity, shape=(512, 512), weighting=[1, 1, 1]):
        super(FunctionGenerator, self).__init__(name, arity)
        self.__fill_set()
        self.__setup()
        self.weights = weighting

        # Setting up array that is used to generate the images
        self.shape = shape
        self.dX, self.dY = shape
        self.xArray = np.linspace(0.0, self.dX, self.dX).reshape((1, self.dX, 1))
        self.yArray = np.linspace(0.0, self.dY, self.dY).reshape((self.dY, 1, 1))

    def __fill_set(self):
        """
            fill pset with possible nodes
        """
        self.addPrimitive(mathematics.sin, 1)
        self.addPrimitive(mathematics.cos, 1)
        self.addPrimitive(mathematics.arctan, 1)
        self.addPrimitive(mathematics.round, 1)
        self.addPrimitive(mathematics.gradientmag, 1)
        self.addPrimitive(mathematics.erosion, 1)
        self.addPrimitive(mathematics.dilation, 1)
        self.addPrimitive(mathematics.blur, 1)
        self.addPrimitive(mathematics.gradientx, 1)
        self.addPrimitive(mathematics.bilateral, 1)
        self.addPrimitive(mathematics.orient, 1)
        self.addPrimitive(mathematics.add, 2)
        self.addPrimitive(mathematics.sub, 2)
        self.addPrimitive(mathematics.mul, 2)
        self.addPrimitive(mathematics.degrees, 1)
        self.addPrimitive(mathematics.sign, 1)
        self.addPrimitive(mathematics.xor, 2)
        self.addPrimitive(mathematics.bitor, 2)
        self.addPrimitive(mathematics.bitand, 2)

    def __setup(self):
        """
            Setting up GP
        """
        creator.create("FitnessMin", base.Fitness, weights=(-1.0,))
        creator.create("Individual", list, fitness=creator.FitnessMin)
        creator.create("subIndividual", gp.PrimitiveTree, fitness=creator.FitnessMin,
                       pset=self)

        self.toolbox = base.Toolbox()
        self.toolbox.register("expr", gp.genHalfAndHalf, pset=self, min_=1, max_=10)
        self.toolbox.register("subindividual", tools.initIterate, creator.subIndividual, self.toolbox.expr)
        self.toolbox.register("individual", self.color_ind, creator.Individual)
        self.toolbox.register("population", tools.initRepeat, list, self.toolbox.individual)
        self.toolbox.register("mate", self.color_mate)
        self.toolbox.register("mutate1", self.color_mutate1)
        self.toolbox.register("mutate2", self.color_mutate2)
        self.toolbox.register("mutate3", self.color_mutate3)
        self.toolbox.register("mutate4", self.color_mutate4)

    def sub_lists(self):
        #Setting up Individual by creating three Primitive Trees
        list = []
        for _ in range(3):
            list.append(self.toolbox.subindividual())
        return list

    def color_ind(self, Ind):
        #Creating Individuals
        return tools.initIterate(Ind, self.sub_lists)

    def __generate_image(self, ind, name):
        """
            Generates image from GP individual
        """
        ls = []
        for z, f in enumerate(ind):
            weight = self.weights[z]
            func = gp.compile(f, self)
            ls.append(weight * np.array(map(func, [self.xArray],[self.yArray]))[0])
            #(ls[z].shape, "pre-tile")
            ls[z] = np.tile(ls[z], (self.dX / ls[z].shape[0], self.dY / ls[z].shape[1], 1/ ls[z].shape[2]))
            #(ls[z].shape, "layer")
        imgls = np.array(ls)
        #(imgls.shape, "shape")
        img = np.uint8(np.rint(imgls * 255.0)).transpose()[0]
        #(img.shape, "after transpose")
        Image.fromarray(img).save(name)

    def select(self, pop):
        """
            Calls interface to select the 2 individuals that should mate
        """
        dictPop = {}
        for i, ind in enumerate(pop):
            dictPop["./Ideas/images/" + str(i) + ".jpg"] = ind
            self.__generate_image(ind, "./Ideas/images/" + str(i) + ".jpg")
        self.names = interface.Interface.runWindow()
        return [dictPop[name] for name in self.names]

    """
    Mutation and crossover algorithm adjusted to work on the sub-individuals
    """
    def color_mutate1(self, ind):
        for i in ind:
            i = gp.mutInsert(i, self)[0]
        return ind

    def color_mutate2(self, ind):
        for i in ind:
            i = gp.mutShrink(i)[0]
        return ind

    def color_mutate3(self, ind):
        for i in ind:
            i = gp.mutNodeReplacement(i, pset = self)[0]
        return ind

    def color_mutate4(self, ind):
        for i in ind:
             i = gp.mutUniform(i, pset = self, expr= self.toolbox.expr)[0]
        return ind

    def color_mate(self, ind1, ind2, p):
        for i1, i2 in zip(ind1, ind2):
            gp.cxOnePointLeafBiased(i1, i2, p)
