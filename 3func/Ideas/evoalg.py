from copy import deepcopy
from EI import FunctionGenerator

"""
Method that can be called to run an experiment
"""

def run(shape = (512,512), num_epochs = 30):
    gener = FunctionGenerator("MAIN", arity=2, shape = shape, weighting=[1,1,1])
    pop = gener.toolbox.population(12)

    for g in range(num_epochs):
        print(g)
        offspring = gener.select(pop)
        double = deepcopy(offspring[:2])
        child1, child2 = double
        gener.toolbox.mate(child1, child2, 0.1)
        offspring.extend(double)

        double = deepcopy(offspring[:2])
        offspring.extend([gener.toolbox.mutate1(double[0]), gener.toolbox.mutate2(double[0])])

        double = deepcopy(offspring[:2])
        offspring.extend([gener.toolbox.mutate3(double[0]), gener.toolbox.mutate4(double[0])])

        double = deepcopy(offspring[:2])
        offspring.extend([gener.toolbox.mutate1(double[1]), gener.toolbox.mutate2(double[1])])

        double = deepcopy(offspring[:2])
        offspring.extend([gener.toolbox.mutate3(double[1]), gener.toolbox.mutate4(double[1])])

        pop = offspring
