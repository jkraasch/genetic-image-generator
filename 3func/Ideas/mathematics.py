import numpy as np
import cv2
from scipy import ndimage, misc

"""
Symbolic Expressions used in GP
"""

def mul(x, y):
    return np.multiply(x,y)


def add(x, y):
    return x + y


def sub(x, y):
    return x-y


def bitor(x, y):
    if isinstance(x, np.ndarray) or isinstance(y, np.ndarray):
        x = np.asarray(x)
        y = np.asarray(y)
        return np.bitwise_or(x.astype(int),y.astype(int))
    return np.bitwise_or(int(x),int(y))


def mymax(x, y):
    if isinstance(x, np.ndarray) or isinstance(y, np.ndarray):
        return x
    return max(x,y)


def mymin(x, y):
    if isinstance(x, np.ndarray) or isinstance(y, np.ndarray):
        x = np.asarray(x)
        y = np.asarray(y)
        return y
    return np.min(x,y)


def bitand(x, y):
    if isinstance(x, np.ndarray) or isinstance(y, np.ndarray):
        x = np.asarray(x)
        y = np.asarray(y)
        return np.bitwise_and(x.astype(int),y.astype(int))
    x,y = int(x),int(y)
    return np.bitwise_and(x,y)


def xor(x, y):
    if isinstance(x, np.ndarray) or isinstance(y, np.ndarray):
        x = np.asarray(x)
        y = np.asarray(y)
        return np.bitwise_xor(x.astype(int),y.astype(int))
    x,y = int(x),int(y)
    return np.bitwise_xor(x,y)


def sin(x):
    if isinstance(x, np.ndarray):
        return (np.sin(x) + 1) / 2
    return (np.sin(x) + 1) / 2


def cos(x):
    if isinstance(x, np.ndarray):
        return (np.cos(x) + 1) / 2
    return (np.cos(x) + 1) / 2


def arctan(x):
    if isinstance(x, np.ndarray):
        return np.arctan(x)
    return np.arctan(x)


def sign(x):
    if isinstance(x, np.ndarray):
        return np.sign(x)
    return np.sign(x)


def log(x):
    x = np.nan_to_num(x)
    #("LOGGING")
    if isinstance(x, np.ndarray):
        return np.log1p(x)
    return np.log1p(x)


def sinc(x):
    #("sincing")
    if isinstance(x, np.ndarray):
        return np.sinc(x)
    return np.sinc(x)


def degrees(x):
    if isinstance(x, np.ndarray):
        return np.degrees(x)
    return np.degrees(x)


def new_abs(x):
    return np.abs(x)


def round(x):
    return np.around(x)


def blur(img):
    dX, dY, dZ = img.shape
    #("blurring", img.shape)
    img = cv2.blur(img, ksize = (3,3)).reshape((dX,dY,dZ))
    #(img.shape)
    return img


def gradientx(img):
    #('sobellings', img.shape)
    dX, dY, dZ = img.shape
    img = ndimage.sobel(img.transpose()).transpose()
    #(img.shape)
    return img


def gradientmag(img):
    #("maging")
    return ndimage.gaussian_gradient_magnitude(img, 1)


def erosion(img):
    #("ero")
    _,_,dZ = img.shape
    return ndimage.grey_erosion(img, size = (3,3,dZ))


def dilation(img):
    #("dil")
    _,_,dZ = img.shape
    return ndimage.grey_dilation(img, size = (3,3,dZ))


def bilateral(img):
    #("BILATERAL")
    #(img.shape)
    dX, dY, dZ = img.shape
    img = cv2.bilateralFilter(img.astype(np.float32), 15, 200,200).reshape(dX,dY,dZ)
    #(img.shape)
    return img


def orient(img):
    #(img.shape, "orient")
    dX,dY,dZ = img.shape
    # Filter the blurred grayscale images using filter2D
    img = img.astype(np.float32)
    sobel_x = np.array([[-1, 0, 1],
                        [-2, 0, 2],
                        [-1, 0, 1]])

    sobel_y = np.array([[-1, -2, -1],
                        [0, 0, 0],
                        [1, 2, 1]])
    filtered_blurred_x = cv2.filter2D(img, cv2.CV_32F, sobel_x)
    filtered_blurred_y = cv2.filter2D(img, cv2.CV_32F, sobel_y)

    # Compute the orientation of the image
    orien = cv2.phase(np.array(filtered_blurred_x, np.float32), np.array(filtered_blurred_y, dtype=np.float32),
                      angleInDegrees=True).reshape(dX,dY,dZ)
    #("Orientation")
    return orien
