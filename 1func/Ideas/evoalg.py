from copy import deepcopy
import Ideas.EI

"""
Method that can be called to run an experiment
"""

def run(shape = (512,512), num_epochs = 30):
    gener = Ideas.EI.FunctionGenerator("MAIN", arity=2, shape=shape)

    pop = gener.toolbox.population(12)

    for i in range(num_epochs):
        print(i)

        offspring = gener.toolbox.select(pop)

        double = deepcopy(offspring[:2])
        child1, child2 = double
        gener.toolbox.mate(child1,child2)

        offspring.extend([gener.toolbox.mutate1(double[0], gener)[0], gener.toolbox.mutate2(double[1], gener)[0]])

        double = deepcopy(offspring[:2])
        child1, child2 = double
        gener.toolbox.mate(child1,child2)

        offspring.extend([gener.toolbox.mutate3(double[0])[0], gener.toolbox.mutate4(double[1])[0]])

        double = deepcopy(offspring[:2])
        child1, child2 = double
        gener.toolbox.mate(child1,child2)

        offspring.extend([gener.toolbox.mutate5(double[0],pset = gener)[0], gener.toolbox.mutate1(double[1], gener)[0]])

        double = deepcopy(offspring[:2])
        child1, child2 = double
        gener.toolbox.mate(child1,child2)

        offspring.extend([gener.toolbox.mutate2(double[0], gener)[0], gener.toolbox.mutate5(double[1], pset = gener)[0]])

        offspring.extend(gener.toolbox.population(2))

        pop = offspring

