import numpy as np
import cv2
from scipy import ndimage, misc

"""
Symbolic Expressions used in GP
"""


def three():
    return np.around(np.random.rand(1,1,3))

def mul(x, y):
    if isinstance(x, np.ndarray) or isinstance(y, np.ndarray):
        return np.multiply(x,y)
    return np.array([np.multiply(x,y),np.multiply(x,y),np.multiply(x,y)])

def mod(x, y):
    if isinstance(x, np.ndarray) or isinstance(y, np.ndarray):
        return np.mod(x,y)
    return np.array([np.mod(x,y),np.mod(x,y),np.mod(x,y)])

def inv(x):
    #print("inv")
    return np.invert(x.astype(np.uint))

def invert(x):
    #print('inveting')
    x = np.divide(x, 1, where= x != 0)
    return x


def div(x, y):
    if isinstance(x, np.ndarray) or isinstance(y, np.ndarray):
        return np.divide(x,y)

    return np.array([np.divide(x,y),np.divide(x,y),np.divide(x,y)])


def add(x, y):
    if isinstance(x, np.ndarray) or isinstance(y, np.ndarray):
        return x + y
    return np.array([x + y, x + y, x + y])


def sub(x, y):
    if isinstance(x, np.ndarray) or isinstance(y, np.ndarray):
        return x - y
    return np.array([x - y, x - y, x - y])

def bitor(x, y):
    if isinstance(x, np.ndarray) or isinstance(y, np.ndarray):
        x = np.asarray(x)
        y = np.asarray(y)
        return np.bitwise_or(x.astype(int),y.astype(int))
    return np.array([np.bitwise_or(int(x),int(y)),np.bitwise_or(int(x),int(y)),np.bitwise_or(int(x),int(y))])

def mymax(x, y):
    if isinstance(x, np.ndarray) or isinstance(y, np.ndarray):
        return x
    return np.array([max(x,y),max(x,y),max(x,y)])

def mymin(x, y):
    if isinstance(x, np.ndarray) or isinstance(y, np.ndarray):
        x = np.asarray(x)
        y = np.asarray(y)
        return y
    return np.array([np.min(x,y),np.min(x,y),np.min(x,y)])





def bitand(x, y):
    if isinstance(x, np.ndarray) or isinstance(y, np.ndarray):
        x = np.asarray(x)
        y = np.asarray(y)
        return np.bitwise_and(x.astype(int),y.astype(int))
    x,y = int(x),int(y)
    return np.array([np.bitwise_and(x,y),np.bitwise_and(x,y),np.bitwise_and(x,y)])


def xor(x, y):
    if isinstance(x, np.ndarray) or isinstance(y, np.ndarray):
        x = np.asarray(x)
        y = np.asarray(y)
        return np.bitwise_xor(x.astype(int),y.astype(int))
    x,y = int(x),int(y)
    return np.array([np.bitwise_xor(x,y),np.bitwise_xor(x,y),np.bitwise_xor(x,y)])


def sin(x):
    if isinstance(x, np.ndarray):
        return (np.sin(x) + 1) / 2
    return np.array([(np.sin(x) + 1) / 2, (np.sin(x) + 1) / 2, (np.sin(x) + 1) / 2])


def cos(x):
    if isinstance(x, np.ndarray):
        return (np.cos(x) + 1) / 2
    return np.array([(np.cos(x) + 1) / 2, (np.cos(x) + 1) / 2, (np.cos(x) + 1) / 2])


def arctan(x):
    if isinstance(x, np.ndarray):
        return np.arctan(x)
    return np.array([np.arctan(x), np.arctan(x), np.arctan(x)])


def sign(x):
    if isinstance(x, np.ndarray):
        return np.sign(x)
    return np.array([np.sign(x), np.sign(x), np.sign(x)])


def log(x):
    x = np.nan_to_num(x)
    #("LOGGING")
    if isinstance(x, np.ndarray):
        return np.log1p(x)
    return np.array([np.log1p(x), np.log1p(x), np.log1p(x)])


def sinc(x):
    #("sincing")
    if isinstance(x, np.ndarray):
        return np.sinc(x)
    return np.array([np.sinc(x), np.sinc(x), np.sinc(x)])


def degrees(x):
    if isinstance(x, np.ndarray):
        return np.degrees(x)
    return np.array([np.degrees(x), np.degrees(x), np.degrees(x)])


def new_abs(x):
    if isinstance(x, np.ndarray):
        return np.abs(x)
    return np.array([np.abs(x), np.abs(x), np.abs(x)])


def round(x):
    if isinstance(x, np.ndarray):
        return np.around(x)
    return np.array([np.around(x), np.around(x), np.around(x)])

def blur(img):
    dX, dY, dZ = img.shape
    #("blurring", img.shape)
    img = cv2.blur(img, ksize = (3,3)).reshape((dX,dY,dZ))
    #(img.shape)
    return img

def gradientx(img):
    #('sobellings', img.shape)
    dX, dY, dZ = img.shape
    img = ndimage.sobel(img.transpose()).transpose()
    #(img.shape)
    return img

def gradientmag(img):
    #("maging")
    return ndimage.gaussian_gradient_magnitude(img, 3)

def erosion(img):
    #("ero")
    _,_,dZ = img.shape
    return ndimage.grey_erosion(img, size = (3,3,dZ))

def dilation(img):
    #("dil")
    _,_,dZ = img.shape
    return ndimage.grey_dilation(img, size = (3,3,dZ))

def bilateral(img):
    #("BILATERAL")
    dX, dY, dZ = img.shape
    img = cv2.bilateralFilter(img.astype(np.float32), 15, 200,200).reshape(dX,dY,dZ)
    #(img.shape)
    return img

def orient(img):
    #(img.shape, "orient")
    dX,dY,dZ = img.shape
    # Filter the blurred grayscale images using filter2D
    img = img.astype(np.float32)
    sobel_x = np.array([[-1, 0, 1],
                        [-2, 0, 2],
                        [-1, 0, 1]])

    sobel_y = np.array([[-1, -2, -1],
                        [0, 0, 0],
                        [1, 2, 1]])
    filtered_blurred_x = cv2.filter2D(img, cv2.CV_32F, sobel_x)
    filtered_blurred_y = cv2.filter2D(img, cv2.CV_32F, sobel_y)

    # Compute the orientation of the image
    orien = cv2.phase(np.array(filtered_blurred_x, np.float32), np.array(filtered_blurred_y, dtype=np.float32),
                      angleInDegrees=True).reshape(dX,dY,dZ)
    #("Orientation")
    return orien

def linear_gradient(img):
    s = [0,0,0]
    f = [255,255,255]
    n = img.shape[1]
    ''' returns a gradient list of (n) colors between
    two hex colors. start_hex and finish_hex
    should be the full six-digit color string,
    inlcuding the number sign ("#FFFFFF") '''
    # Starting and ending colors in RGB form
    # Initilize a list of the output colors with the starting color
    RGB_list = [s]
    ##('grading-----------------------------------------------------')
    # Calcuate a color at each evenly spaced value of t from 1 to n
    for t in range(1, n):
    # Interpolate RGB vector for color at the current value of t
        curr_vector = [
          int(s[j] + (float(t)/(n-1))*(f[j]-s[j]))
          for j in range(3)
        ]
        # Add it to our list of output colors
        RGB_list.append(np.asarray(curr_vector).transpose().tolist())

    return img * (np.tile(RGB_list,(img.shape[0],1,1))/255)
