from PIL import Image
from deap import base
from deap import creator
from deap import gp
from deap import tools
import numpy as np
import interface.Interface
import mathematics


class FunctionGenerator(gp.PrimitiveSet):

    def __init__(self, name, arity, shape = (1028,1028)):
        super(FunctionGenerator, self).__init__(name, arity)

        #Setting up array that is used to generate the images
        self.dX, self.dY = shape
        self.xArray = np.linspace(0.0, self.dX, self.dX).reshape((1, self.dX, 1))
        self.yArray = np.linspace(0.0, self.dY, self.dY).reshape((self.dY, 1, 1))

        self.__fill_set()
        self.__setup()

    def __fill_set(self):
        """
        fill pset with possible nodes
        """
        # Needs to be here otherwise cant work with arrays
        self.addPrimitive(np.array, 1)

        self.addPrimitive(mathematics.sin, 1)
        self.addPrimitive(mathematics.cos, 1)
        self.addPrimitive(mathematics.arctan, 1)
        self.addPrimitive(mathematics.round, 1)
        self.addPrimitive(mathematics.gradientmag, 1)
        self.addPrimitive(mathematics.erosion, 1)
        self.addPrimitive(mathematics.dilation, 1)
        self.addPrimitive(mathematics.blur, 1)
        self.addPrimitive(mathematics.gradientx, 1)
        self.addPrimitive(mathematics.bilateral, 1)
        self.addPrimitive(mathematics.orient, 1)
        self.addPrimitive(mathematics.add, 2)
        self.addPrimitive(mathematics.sub, 2)
        self.addPrimitive(mathematics.mul, 2)
        self.addPrimitive(mathematics.linear_gradient, 1)
        self.addPrimitive(mathematics.degrees, 1)
        self.addPrimitive(mathematics.sign, 1)
        self.addPrimitive(mathematics.xor, 2)
        self.addPrimitive(mathematics.bitor, 2)
        self.addPrimitive(mathematics.bitand, 2)

        self.addEphemeralConstant(name = "threedim", ephemeral= mathematics.three)

    def __setup(self):
        """
        Setting up GP
        """
        creator.create("FitnessMin", base.Fitness, weights=(-1.0,))
        creator.create("Individual", gp.PrimitiveTree, fitness=creator.FitnessMin, pset=self)
        self.toolbox = base.Toolbox()
        self.toolbox.register("expr", gp.genHalfAndHalf, pset=self, min_=1, max_=7)
        self.toolbox.register("individual", tools.initIterate, creator.Individual, self.toolbox.expr)
        self.toolbox.register("population", tools.initRepeat, list, self.toolbox.individual)
        self.toolbox.register("mutate1", gp.mutNodeReplacement)
        self.toolbox.register("mutate2", gp.mutInsert)
        self.toolbox.register("mutate3", gp.mutShrink)
        self.toolbox.register("mutate3", gp.mutEphemeral, mode = "one")
        self.toolbox.register("mutate4", gp.mutEphemeral, mode = "all")
        self.toolbox.register("mutate5", gp.mutUniform, expr = self.toolbox.expr)
        self.toolbox.register("mate", gp.cxOnePoint)
        self.toolbox.register("select", self.select_pop)


    def __genImg(self,ind, name):
        """
        Generates image from GP individual
        """
        func = gp.compile(ind, self)
        img = map(func, [self.xArray],[self.yArray])[0]
        img = np.uint8(img)
        img = np.tile(img, (self.dX / img.shape[0], self.dY / img.shape[1], 3 / img.shape[2]))
        Image.fromarray(img).save("./Ideas/images/" + str(name) + ".jpg")


    def select_pop(self, pop):
        """
        Calls interface to select the 2 individuals that should mate
        """
        dictPop = {}
        for i, ind in enumerate(pop):
            dictPop["./Ideas/images/" + str(i) + ".jpg"] = ind
            self.__genImg(ind,i)
        names = interface.Interface.runWindow()
        return [dictPop[name] for name in names]
